XAMPP version = 5.6.12
OwnCloud version = 8.0.16 (8.1 or higher doesnt support windows 10)
php version = 5.6.12
java version = 1.8
Public IP = 86.14.20.144

XAMPP port = 80
OwnCloud port = 80
Jenkins port = 8080
Minecraft port = 19132
SonarQube port = 9000

================================================================
OWNCLOUD
================================================================
owncloud admin
user = owncloudadmin
pass = 20peter&SON18

owncloud admin account
user = admin
pass = 20peter&SON18


Generating an ssl key:
openssl genrsa -des3 -out server.key 1024
password: 20peter&SON18

Creates an unencrypted version of the key so apps can read it:
openssl rsa -in server.key -out server.pem

This uses the key to fill out the info for a certificate to say your server is safe:
openssl req -new -key server.key -out server.csr

Creates the actual certificate, you have to specifiy in days how long it will last for:
openssl x509 -req -days 1000 -in server.csr -signkey server.key -out server.crt

Updated the httpd-ssl.conf file to point to the new certificate created. This can be found
in apache/conf/ssl/

Changed my IP address to be static. I did this in this dir: 
Control Panel\Network and Internet\Network Connections
The default gateway and DNS server should be the same. The static IP should also be
the same except for the last 3 digits.
= was unable to get this to work.


Complete the rest of the video when you want to open it up on the web.

Helpful notes:
https://www.youtube.com/watch?v=H4hMAv9yQ8A
https://www.youtube.com/watch?time_continue=409&v=C6y_g5v6CKc
https://www.youtube.com/watch?v=duitxP__Bf0


BUGS:
Missing requirements.
Please make sure that OpenSSL together with the PHP extension is
enabled and properly configured. For now, the encryption 
app has been disabled.



================================================================
MINECRAFT
================================================================
Server has to be on the bedrock additon. This is Alpha stage.

Still unable to connect. Need to try and 
learn how to connect on same pc. Then try to branch out.

1. Download and unzip.
2. Run the exe for all the files to generate.
3. Now close the exe.
4. If you wanna use this on localhost, need to enable loopback for minecraft. 
	Can do this by using this command in the cmd:
	CheckNetIsolation.exe LoopbackExempt -a -p=S-1-15-2-1958404141-86561845-1752920682-3514627264-368642714-62675701-733520436

Playing on LAN (same local network)
Players on the same local area network (LAN) as the server can connect to it via the Friends tab.
The server will appear as a local multiplayer world.
1. Connect both PC's together with an ethernet cable.
2. Open the cmd and type in: 'ipconfig'.
3. Look for the ipconfig called: 'Ethernet adapter Ethernet'. In this section you are given an ip address.
	This is the IP address to use to connect to the server.
	You can test this connect by typing this into the cmd: ping <IP of other PC for 'Ethernet adapter Ethernet'> -t
		1. Launch Minecraft.
		2. Open server tab. New server, enter a name you want, enter the IP for lan we found above.
			Then enter in the port used in the minecraft server. It says in the cmd line when you run it.
		3. Now save and you should be able to connect. This server will now also appear in your friends tab.
		4. Navigate to Friends tab.
		5. Select the server from the joinable worlds list.

Helpful notes:
- https://www.reddit.com/user/ProfessorValko/comments/9f438p/bedrock_dedicated_server_tutorial/
- When entering a server name, you can use any name you want, only the ip matters


================================================================
JENKINS
================================================================
Jenkins log in as admin:
username: admin
password: 20peter&SON18