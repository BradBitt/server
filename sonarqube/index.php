<?php
$port = '9000';
header('Location: '
    . ($_SERVER['HTTPS'] ? 'https' : 'http')
    . '://' . $_SERVER['HTTP_HOST'] . ':' . $port
    . $_SERVER['REQUEST_URI']);
exit;
?>